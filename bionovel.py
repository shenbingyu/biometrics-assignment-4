#!/bin/sh

#  bio.py
#  biometrics
#
#  Created by bingyu shen on 12/5/15.
#

import os
import sys
import datetime

sys.path.append('/Users/bingyushen/Downloads/libsvm-3.20/python')

from svmutil import *

#1###########################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_EcoFlex_Silgum_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_EcoFlex_Silgum.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_EcoFlex_Silgum_T_novel.txt')
m = svm_train(y, x,'-c 128')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
#2##############################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_Gelatine_EcoFlex_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_Gelatine_EcoFlex.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_Gelatine_EcoFlex_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
##3################################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_Gelatine_Latex_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_Gelatine_Latex.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_Gelatine_Latex_T_novel.txt')
m = svm_train(y, x,'-c 128')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
###4###################################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_Gelatine_WoodGlue_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_Gelatine_WoodGlue.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_Gelatine_WoodGlue_T_novel.txt')
m = svm_train(y, x,'-c 8')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
####5######################################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_Latex_EcoFlex_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_Latex_EcoFlex.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_Latex_EcoFlex_T_novel.txt')
m = svm_train(y, x,'-c 8')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
#6###################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_Silgum_Gelatine_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_Silgum_Gelatine.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_Silgum_Gelatine_T_novel.txt')
m = svm_train(y, x,'-c 8')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
#7######################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_Silgum_Latex_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_Silgum_Latex.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_Silgum_Latex_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
#######8######
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_WoodGlue_EcoFlex_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_WoodGlue_EcoFlex.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_WoodGlue_EcoFlex_T_novel.txt')
m = svm_train(y, x,'-c 2')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
##9####################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_WoodGlue_Latex_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_WoodGlue_Latex.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_WoodGlue_Latex_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)
#10###########################
f=open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_WoodGlue_Silgum_T_novel_result.txt",'a')
y, x = svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Train_WoodGlue_Silgum.txt')
y1, x1= svm_read_problem('/Users/bingyushen/Downloads/LBP_features/Test_WoodGlue_Silgum_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
    i=i+1
    if i<=1000:
        print len(p_val)
        list="1"+","+str(info[0])+"\n"
        f.write(list)
    else:
        list="-1"+","+str(info[0])+"\n"
        f.write(list)

