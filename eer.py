#!/bin/sh

#  eer.py
#  
#
#  Created by bingyu shen on 12/7/15.
#
import matplotlib.pyplot as plt
import numpy as np

FRR=[]
FAR=[]
TAR=[]
TRR=[]
X=[]
Y=[]
diff=[]
for threshold_big in range(-1000,2000,1):
    with open("/Users/bingyushen/Desktop/biometrics/biometrics/Test_WoodGlue_Silgum_T_novel_result.txt") as f:
        threshold=float(threshold_big)/100
        
        TAnum=0
        FRnum=0
        FAnum=0
        TRnum=0

        for line in f:
            if line[0]=="1":
                if float(line[2:])<=threshold:
                    FRnum=FRnum+1
                else:
                    TAnum=TAnum+1
        
            elif line[:2]=="-1":
                if float(line[3:])>=threshold:
                    FAnum=FAnum+1
                else:
                    TRnum=TRnum+1

    FRR.append(float(FRnum)/float(1000))
    FAR.append(float(FAnum)/float(400))
    TAR.append(float(TAnum)/float(1000))
    TRR.append(float(TRnum)/float(400))
    diff.append(abs(float(FRnum)/float(1000)-float(FAnum)/float(400)))

for i in range(len(diff)):
    if diff[i]<diff[i-1]:
        temp=i

print FRR[temp]
print FAR[temp]

plt.plot(FAR,FRR,'g-',label='GUA')
plt.ylabel('False Positive Rate')
plt.xlabel('False Negative Rate')
plt.legend()
plt.savefig("det.png")


plt.grid(True)
plt.show()